![gentoo](https://gentoo.org/assets/img/badges/gentoo-badge2.png "gentoo")

# about

fnordpipe-overlay is an overlay for gentoo's build system (portage).
it extends gentoo with additional packages and profiles.

# documentation

* [gentoo devmanual](https://devmanual.gentoo.org)
  * [ebuild writing](https://devmanual.gentoo.org/ebuild-writing/index.html)
  * [variables](https://devmanual.gentoo.org/ebuild-writing/variables/index.html)
  * [install functions](https://devmanual.gentoo.org/function-reference/install-functions/index.html)
* `man 5 ebuild`
* `man portage`
